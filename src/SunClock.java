/**=======================================================================*
 *                                                                        *
 * Copyright (C) 2015 Carlos Augusto de Souza Braga                       *
 * <CASBraga@Gmail.com>                                                   *
 *                                                                        *
 *  This program is free software: you can redistribute it and/or modify  *
 *  it under the terms of the GNU General Public License as published by  *
 *  the Free Software Foundation, either version 3 of the License, or     *
 *  (at your option) any later version.                                   *
 *                                                                        *
 *  This program is distributed in the hope that it will be useful,       *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *  GNU General Public License for more details.                          *
 *========================================================================* 
 * 
 * Class MyPApplet 
 * Extends PApplet data type from Processing package
 * 
 * Creates an applet with an image as a background and allows to draw on 
 * this created canvas
 * 
 * contains
 * 
 * Global variables:
 *     URL             => string containing the url to background image;
 *                        can be a local image or a remote one
 *     backgroundImage => PImage object to receive the background image
 *                        and set it up on the canvas
 * 
 * Methods:
 *     private:
 *         sunColor => sets the color of the sun clock 
 *                     using the seconds, minutes or 
 *                     hours
 *     public:
 *         setup    => sets up the canvas; will be 
 *                     called only once
 *         draw     => draws elements to the canvas; will be 
 *                     called multiple times 
 */

import java.lang.Math;
import processing.core.*;

public class SunClock extends PApplet {
	// global variables
	private String URL = "http://cseweb.ucsd.edu/~minnes/palmTrees.jpg";
	private PImage backgroundImg;
	
	// methods
	// private methods (helper functions)
	private int[] sunColor(int time, String unit){
		// local variables
		// color is an intger array containing the color indexes
		int[] color = new int[3];
		
		// selects which of the timing functions to use
		// based on the unit parameter
		switch(unit){
		   case "second":
			   // frequency is 60 Hz (1/60 s)
			   color[0] = (int) ((float) 255 * pow(sin((float) time / 60), 2));
			   color[1] = (int) ((float) 255 * pow(sin((float) time / 60), 2));
			   color[2] = 0;
			   break;
		   case "minute":
			   // frequency is 1/6 m
			   color[0] = (int) ((float) 255 * pow(sin((float) time / 60), 2));
			   color[1] = (int) ((float) 255 * pow(sin((float) time / 60), 2));
			   color[2] = 0;
			   break;
		   case "hour"  :
			   // frequency is 1/24 h
			   color[0] = (int) ((float) 255 * pow(sin((float) time / 24), 2));
			   color[1] = (int) ((float) 255 * pow(sin((float) time / 24), 2));
			   color[2] = 0;
			   break;
		   default      :
			   // uses unit = second configuration by default
			   color[0] = (int) ((float) 255 * pow(sin((float) time / 60), 2));
			   color[1] = (int) ((float) 255 * pow(sin((float) time / 60), 2));
			   color[2] = 0;
			   break;
		}
		
		return color;
	}
	
	// public methods (event handlers)
	public void setup(){
		// sets up a 200 X 200 pixels canvas and loads the
		// URL image to be used as background
		size(200, 200);
		backgroundImg = loadImage(URL, "jpg");
	}
	
	public void draw(){
		// local variables
		// initializes the color indexes using sunColor method
		// in this case, time is in seconds
		int[] rgb = sunColor(second(), "second"); 
		
		// resize background image and
		// draws it in the canvas
		backgroundImg.resize(0, height);
        image(backgroundImg, 0, 0);
        
        // draws the yellow sun visual clock
        fill(rgb[0], rgb[1], rgb[2]);
        noStroke();
        ellipse(width / 4, height / 5, width / 5, height / 5);
	}

}