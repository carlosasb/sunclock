/*=======================================================================*
*                                                                        *
* Copyright (C) 2015 Carlos Augusto de Souza Braga                       *
* <CASBraga@Gmail.com>                                                   *
*                                                                        *
*  This program is free software: you can redistribute it and/or modify  *
*  it under the terms of the GNU General Public License as published by  *
*  the Free Software Foundation, either version 3 of the License, or     *
*  (at your option) any later version.                                   *
*                                                                        *
*  This program is distributed in the hope that it will be useful,       *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*  GNU General Public License for more details.                          *
*========================================================================*/

This is the README file for the java project SunClock. This is, at the moment,
composed of the SunClock.java class. As the name suggests, this draws a sun
(a circle) that changes color as the time tick by. Currently, one has to manually
(i.e. inside the code) change the timer method which it uses.

This is an Eclipse project and can be directly imported to Eclipse.
This project is dependent on the Processing libraries (core.jar to be precise).
The library can be downloaded at "https://processing.org/download/" and imported 
directly into this project for use following the instructions at 
"https://processing.org/tutorials/eclipse/".

TODO:

Add user input for the timer. Buttons and other things.

Add a way for the user to change the default picture. This may
take a while since I am still learning processing.